﻿using JWT_NET5.Dtos;
using JWT_NET5.Srevices.Authentication;
using JWT_NET5.Srevices.Operation;
using Microsoft.AspNetCore.Mvc;

namespace JWT_NET5.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class OperationController:ControllerBase
    {
        private readonly IOperationRepository _operationRepository;
        private readonly IUserService _userService;

        public OperationController(IOperationRepository operationRepository,IUserService userService)
        {
            _operationRepository= operationRepository;
            _userService = userService;
        }
        
        [HttpPost]
        public IActionResult Import(ImportDto dto)
        {
                _operationRepository.AddImportoperation(new PostImportDto()
                {
                    ImportDto = dto,
                    ManagerId = new Guid(_userService.GetUserId())
                });

            return Ok();
        }

    }
}
