﻿using JWT_NET5.Dtos;
using JWT_NET5.Etities;
using JWT_NET5.Models;
using JWT_NET5.Srevices.Authentication;
using JWT_NET5.Srevices.ManagersRepository;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace JWT_NET5.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class ManagerController : ControllerBase
    {
        private readonly IManagerRepository _managerRepository;
        private readonly IAuthService _authService;
        private readonly UserManager<Manager> _userManager;

        public ManagerController(IManagerRepository managerRepository, IAuthService authService, UserManager<Manager> userManager)
        {
            _managerRepository = managerRepository;
            _authService = authService;
            _userManager = userManager;
        }

        [HttpPost]
        public async Task<IActionResult> AddManager([FromBody] RegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _authService.RegisterAsync(new Manager() { Name = model.UserName }, model.Password);

     

            return Ok(result);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ManagerDto>>> GetManagers()
        {
            var managersFromRepo = await _managerRepository.GetManagersAsync();


  
            return Ok(managersFromRepo.Select(s=>new ManagerDto() {Id = s.Id, Name=s.Name,Email=s.Email}));
        }
    }
}
