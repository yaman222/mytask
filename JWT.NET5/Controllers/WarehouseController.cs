﻿using JWT_NET5.Etities;
using JWT_NET5.Srevices.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using JWT_NET5.Dtos;
using JWT_NET5.Srevices.WarehousesRepository;
using JWT_NET5.Srevices.ManagersRepository;

namespace JWT_NET5.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class WarehouseController : ControllerBase
    {
        private readonly IWarehouseRepository _warehouseRepository;
        private readonly IUserService _userService;
        
        public WarehouseController(
             IWarehouseRepository warehouseRepository,IUserService userService)
        {
            _warehouseRepository = warehouseRepository;
            _userService = userService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<GetPostWarehouseDto>> GetAllWarehouses()
        {
            return Ok(_warehouseRepository.GetWarehouses());
        }

        [HttpPost]
        public IActionResult CreateMainWarehouse(WarehouseDto dto)
        {
            var warehouseToReturn = _warehouseRepository.AddMainWarehouse(new PostWarehouseDto()
            {
                ManagerId = new Guid(_userService.GetUserId()),
                Address = dto.Address,
                Name = dto.Name
            });

            return Ok(warehouseToReturn);
        }

        [HttpGet]
        public IActionResult GetWarehouse(Guid WarehouseId)
        {
            return Ok(_warehouseRepository.GetWarehouse(WarehouseId));
        }
    }
}
