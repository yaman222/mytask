﻿using System.Security.Authentication;
using JWT_NET5.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using JWT_NET5.Etities;
using JWT_NET5.Srevices.Authentication;

namespace JWT_NET5.DpContexts
{
    [Route("api/Auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        private readonly UserManager<Manager> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUserService _userService;

        public AuthController(IAuthService authService, UserManager<Manager> userManager,
            IHttpContextAccessor httpContextAccessor, IUserService userService)
        {
            _authService = authService;
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
            _userService = userService;
        }


        [HttpPost("LogIn")]
        public async Task<IActionResult> LogInAsync([FromBody] LoginDto model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _authService.LogInAsync(model);

            return Ok(result);
        }

        [Authorize]
        [HttpGet("GetId")]
        public Task<IActionResult> GetUserId()
        {
            return Task.FromResult<IActionResult>(Ok(new { UserId = _userService.GetUserId() }));
        }

        // [HttpPost("AddRoleToUser")]
        // public async Task<IActionResult> AddRoleToUserAsync([FromBody] AddRoleToUserModel model)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }
        //
        //     var result = await _authService.AddRoleAsync(model);
        //
        //     if (!string.IsNullOrEmpty(result))
        //     {
        //         return BadRequest(result);
        //     }
        //
        //     return Ok(result);
        // }
    }
}