﻿using Microsoft.AspNetCore.Identity;

namespace JWT_NET5.Etities;
using System.ComponentModel.DataAnnotations;

public class Manager : IdentityUser<Guid>
{
    public string Name { get; set; }
    
    public ICollection<Warehouse> Warehouses { get; set; }
        = new List<Warehouse>();
}