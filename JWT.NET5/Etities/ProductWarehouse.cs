﻿namespace JWT_NET5.Etities;

public class ProductWarehouse : BaseEntity
{
    public double Amount { get; set; }
    
    public  Warehouse Warehouse { get; set; }
    public Guid WarehouseId { get; set; }
    
    public Product Product { get; set; }
    public Guid ProductId { get; set; }
    
}