﻿using JWT_NET5.Etities;

namespace JWT_NET5;

public class Operations
{
    public Guid Id { get; set; }
    public OperationType type { get; set; }
    public DateTime Date { get; set; }
    public double TotalCost { get; set; }
    
    public Warehouse Warehouse { get; set; }
    public Guid warehouseId { get; set; }

    public ICollection<ProductOperation> ProductOperations { get; set; }
       = new List<ProductOperation>();

}

public enum OperationType
{
    Export,
    Import
}