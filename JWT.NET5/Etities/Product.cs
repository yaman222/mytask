﻿namespace JWT_NET5.Etities;
using System.ComponentModel.DataAnnotations;

public class Product :BaseEntity
{
   
    public string Name { get; set; }

    public double Price { get; set; }

    public ICollection<ProductOperation> ProductOperations { get; set; } 
        = new List<ProductOperation>();
}