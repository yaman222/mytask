﻿using System.Collections;

namespace JWT_NET5.Etities;

public class ProductOperation : BaseEntity
{
    public int Amount { get; set; }
    public double Cost { get; set; }

    public Product Product { get; set; }
    public Guid ProductId { get; set; } 
    
    public Operations Operation { get; set; }
    public Guid OperationId { get; set; }
}