﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace JWT_NET5.Etities;

public class BaseEntity
{
    public BaseEntity()
    {
        Id = Guid.NewGuid();
    }
    public Guid Id { get; set; }
}
public class Warehouse : BaseEntity
{

    public string Name { get; set; }
    
    public string? Address { get; set; }
    
    public Manager Manager { get; set; }
    public Guid ManagerId { get; set; }

    public Warehouse? MainWarehouse { get; set; }
    public Guid? MainWarehouseId { get; set; }

    public ICollection<Warehouse> SubWarehouses { get; set; }
        = new List<Warehouse>();

    public ICollection<Operations> Operations { get; set; }
        = new List<Operations>();

    public ICollection<ProductWarehouse> ProductsWarehouses { get; set; }
        = new List<ProductWarehouse>();
}