﻿using JWT_NET5.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using JWT_NET5.Helpers;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Options;
using JWT_NET5.Etities;
using JWT_NET5.Dtos;

namespace JWT_NET5.Srevices.Authentication
{
    public class AuthService : IAuthService
    {
        private readonly UserManager<Manager> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly JWT _jwt;


        public AuthService(UserManager<Manager> userManager, IOptions<JWT> jwt, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _jwt = jwt.Value;
            _roleManager = roleManager;
        }

        public async Task<GetMangerDto> RegisterAsync(Manager manager, string password) // todo 
        {
            if (await _userManager.FindByEmailAsync(manager.Email) is not null)
            {
                return new GetMangerDto { Message = "Email is already registered" };
            }

            if (await _userManager.FindByNameAsync(manager.UserName) is not null)
            {
                return new GetMangerDto { Message = "Username is already registered" };
            }

            var user = new Manager
            {
                UserName = manager.Name,
                Email = manager.Email,
            };

            var result = await _userManager.CreateAsync(user, password);

            if (!result.Succeeded)
            {
                return new GetMangerDto { Message = string.Join(",", result.Errors.Select(e => e.Description)) };
            }

            await _userManager.AddToRoleAsync(user, Roles.Manager);

            var token = await GenerateJwtToken(user);

            return new GetMangerDto
            {
               Id = user.Id,
               Name = user.Name,
               UserName = user.UserName,
               Token = token
            };
        }

        public async Task<GetMangerDto> LogInAsync(LoginDto model)
        {

            var user = await _userManager.FindByEmailAsync(model.Email);

            if (user is null || !await _userManager.CheckPasswordAsync(user, model.Password))
            {
                return new GetMangerDto()
                {
                    Message = "Email or Password is incorrect!",
                };
            }

            var token = await GenerateJwtToken(user);

            return new GetMangerDto
            {
                Id = user.Id,
                Name = user.Name,
                UserName = user.UserName,
                Token = token
            };
          
        }

        // public async Task<string> AddRoleAsync(AddRoleToUserModel model)
        // {
        //     var user = await _userManager.FindByIdAsync(model.UserId);
        //
        //     if (user is null || !await _roleManager.RoleExistsAsync(model.RoleName))
        //     {
        //         return "Invalid User Id Or Role";
        //     }
        //
        //     if (await _userManager.IsInRoleAsync(user, model.RoleName))
        //     {
        //         return "User is already assigned to this role";
        //     }
        //
        //     var result = await _userManager.AddToRoleAsync(user, model.RoleName);
        //
        //     if (result.Succeeded)
        //     {
        //         return string.Empty;
        //     }
        //
        //     return "Something went wrong";
        // }
        //private async Task<JwtSecurityToken> CreateJwtToken(Manager user)
        //{
        //    var userClaims = await _userManager.GetClaimsAsync(user);
        //    var roles=await _userManager.GetRolesAsync(user);
        //    var roleCaims=new List<Claim>();

        //    foreach(var role in roles)
        //    {
        //        roleCaims.Add(new Claim("roles", role));
        //    }

        //    var claims = new[]
        //       {
        //            new Claim(JwtRegisteredClaimNames.Sub,user.UserName),
        //            new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
        //            new Claim(JwtRegisteredClaimNames.Email,user.Email),
        //            new Claim(JwtRegisteredClaimNames.NameId,user.Id)
        //        }
        //       .Union(userClaims)
        //       .Union(roleCaims);

        //    var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwt.Key));

        //    var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);

        //    var jwtSecurityToken = new JwtSecurityToken(
        //        issuer: _jwt.Issuer,
        //        audience: _jwt.Audience,
        //        claims: claims,
        //        expires: DateTime.Now.AddMinutes(_jwt.DurationInMinutes),
        //        signingCredentials: signingCredentials
        //        );

        //    return jwtSecurityToken;
        //}

        public async Task<JwtSecurityToken> GenerateJwtToken(Manager user)
        {
            var userClaims = await _userManager.GetClaimsAsync(user);
            var roles = await _userManager.GetRolesAsync(user);
            var roleCaims = new List<Claim>();

            foreach (var role in roles)
            {
                roleCaims.Add(new Claim("roles", role));
            }

            var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Email, user.Email),
                    new Claim(JwtRegisteredClaimNames.NameId, user.Id.ToString())
                }
                .Union(userClaims)
                .Union(roleCaims);

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwt.Key));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);


            var jwtSecurityToken = new JwtSecurityToken(
                issuer: _jwt.Issuer,
                audience: _jwt.Audience,
                claims: claims,
                expires: DateTime.Now.AddMinutes(_jwt.DurationInMinutes),
                signingCredentials: creds
            );


            return jwtSecurityToken;
        }
    }
}