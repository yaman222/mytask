﻿using JWT_NET5.Etities;
using JWT_NET5.Models;

namespace JWT_NET5.Srevices.Authentication
{
    public interface IAuthService
    {
        Task<GetMangerDto> RegisterAsync(Manager manager, string password);

        Task<GetMangerDto> LogInAsync(LoginDto model);

    }
}
