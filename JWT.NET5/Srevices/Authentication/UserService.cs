﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace JWT_NET5.Srevices.Authentication
{
    public class UserService : IUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string GetUserId()
        {
            return _httpContextAccessor.HttpContext?.User.FindFirstValue("xx");
        }
    }
}
