﻿using System.Security.Claims;

namespace JWT_NET5.Srevices.Authentication
{
    public interface IUserService
    {
        string GetUserId();
    }
}
