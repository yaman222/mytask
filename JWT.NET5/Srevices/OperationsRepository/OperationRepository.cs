﻿using JWT_NET5.DpContexts;
using JWT_NET5.Dtos;
using JWT_NET5.Etities;
using JWT_NET5.Srevices.Operation;
using JWT_NET5.Srevices.ProductsRepository;
using JWT_NET5.Srevices.ProductsWarehousesRepository;
using Microsoft.AspNetCore.Identity;

namespace JWT_NET5.Srevices
{
    public class OperationRepository : IOperationRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<Manager> _userManager;
        private readonly IProductWarehouseRepository _productWarehouseRepository;
        private readonly IProductRepository _productRepository;

        public OperationRepository(ApplicationDbContext context, UserManager<Manager> userManager, 
            IProductWarehouseRepository productWarehouseRepository, IProductRepository productRepository)
        {
            _context = context;
            _userManager = userManager;
            _productWarehouseRepository= productWarehouseRepository;
            _productRepository= productRepository;
        }


        public void AddImportoperation(PostImportDto import)
        {
            
            var operation=new Operations(){Date=DateTime.Now, type=OperationType.Import,warehouseId=import.ImportDto.WarehouseId};

            double TotalCost = 0;

            foreach (var productWithAmount in import.ImportDto.ProductAmount)
            {
                double cost = 0;

                if (_productWarehouseRepository.ProductWithWarehouseExists(productWithAmount.ProductId, import.ImportDto.WarehouseId))
                {
                    var existedProductWithWarehouse=_productWarehouseRepository.GetProductWithWarehouse(productWithAmount.ProductId,import.ImportDto.WarehouseId);

                    existedProductWithWarehouse.Amount += productWithAmount.Amount;

                    _productWarehouseRepository.UpdateAmountOfProductInWarehouse(productWithAmount.ProductId, import.ImportDto.WarehouseId, productWithAmount.Amount);
                }
                else
                {
                    var newProductWithWarehouse=new ProductWarehouse()
                    {
                        ProductId=productWithAmount.ProductId,
                        WarehouseId=import.ImportDto.WarehouseId,
                        Amount=productWithAmount.Amount
                    };

                    _productWarehouseRepository.AddProductToWarehouse(newProductWithWarehouse);
                }

                cost += _productRepository.GetProduct(productWithAmount.ProductId).Price * productWithAmount.Amount;
                TotalCost += cost;

                _context.ProductsOperations.Add(new ProductOperation()
                {
                    Amount=productWithAmount.Amount,
                    ProductId=productWithAmount.ProductId,
                    OperationId=operation.Id,
                    Cost=cost
                });

            }

            operation.TotalCost = TotalCost;

            _context.Operations.Add(operation);

            _context.SaveChanges();
        }
    }
}
