﻿using JWT_NET5.Dtos;
using JWT_NET5.Etities;

namespace JWT_NET5.Srevices.WarehousesRepository
{
    public interface IWarehouseRepository
    {
        IEnumerable<GetPostWarehouseDto> GetWarehouses();
        IEnumerable<Warehouse> GetWarehouses(Guid managerId);
        IEnumerable<Warehouse> GetSubWarehouses(Guid mainWarehouseId, Guid? managerId);
        IEnumerable<Warehouse> GetSubWarehouses();
        Warehouse GetWarehouse(Guid warehouseId, Guid manageId);
        MainWarehouseDto GetWarehouse(Guid warehouseId);
        GetPostWarehouseDto AddMainWarehouse(PostWarehouseDto postWarehouse);
        void AddSubWarehouse(Warehouse mainWarehouse, Warehouse subWarehouse);
        void RemoveWarehouse(Warehouse warehouse);
        void UpdateWarehouse(Warehouse warehouse);
        bool WarehouseExists(Guid warehouseId);
    }
}
