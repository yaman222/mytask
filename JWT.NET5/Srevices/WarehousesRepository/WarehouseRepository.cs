﻿using JWT_NET5.DpContexts;
using JWT_NET5.Dtos;
using JWT_NET5.Etities;

namespace JWT_NET5.Srevices.WarehousesRepository
{
    public class WarehouseRepository : IWarehouseRepository
    {
        private readonly ApplicationDbContext _context;

        public WarehouseRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<GetPostWarehouseDto> GetWarehouses()
        {
            return _context.Warehouses.Select(GetPostWarehouseDto.Selector()).ToList();
        }

        public IEnumerable<Warehouse> GetWarehouses(Guid managerId)
        {
            if (managerId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(managerId));
            }

            return _context.Warehouses.Where(c => c.ManagerId == managerId).ToList();
        }

        public IEnumerable<Warehouse> GetSubWarehouses(Guid mainWarehouseId, Guid? managerId)
        {
            if (managerId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(managerId));
            }

            if (mainWarehouseId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(mainWarehouseId));
            }

            return _context.Warehouses.Where(w => w.ManagerId == managerId && w.MainWarehouseId == mainWarehouseId)
                .ToList();
        }

        public IEnumerable<Warehouse> GetSubWarehouses()
        {
            return _context.Warehouses.Where(w => w.MainWarehouseId != Guid.Empty).ToList();
        }

        public Warehouse GetWarehouse(Guid warehouseId, Guid managerId)
        {
            if (managerId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(managerId));
            }

            if (warehouseId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(warehouseId));
            }

            return _context.Warehouses.Where(w => w.ManagerId == managerId && w.Id == warehouseId).FirstOrDefault();
        }

        public MainWarehouseDto GetWarehouse(Guid warehouseId)
        {

            return _context.Warehouses.Where(w => w.Id == warehouseId)
                .Select(w => new MainWarehouseDto
                {
                    Id = w.Id,
                    Name = w.Name,
                    Address = w.Address,
                    SubWarehouses = w.SubWarehouses.Select(sw => new GetPostWarehouseDto
                    {
                        Name = sw.Name,
                        Address = sw.Address,
                        ManagerId = sw.ManagerId,
                        Id = sw.Id
                    })
                }).First();
        }

        public GetPostWarehouseDto AddMainWarehouse(PostWarehouseDto postWarehouse)
        {
            var warehouseToAdd = new Warehouse()
            {
                Id = Guid.NewGuid(),
                Name = postWarehouse.Name,
                Address = postWarehouse.Address,
                ManagerId = postWarehouse.ManagerId
            };

            _context.Warehouses.Add(warehouseToAdd);
            _context.SaveChanges();
            return new GetPostWarehouseDto()
            {
                Id = warehouseToAdd.Id,
                ManagerId = warehouseToAdd.ManagerId,
                Name = warehouseToAdd.Name,
                Address = warehouseToAdd.Address
            };
        }

        public void AddSubWarehouse(Warehouse mainWarehouse, Warehouse subWarehouse)
        {
            if (mainWarehouse == null)
            {
                throw new ArgumentNullException(nameof(mainWarehouse));
            }

            if (subWarehouse == null)
            {
                throw new ArgumentNullException(nameof(subWarehouse));
            }

            if (subWarehouse.MainWarehouseId == mainWarehouse.Id)
            {
                throw new Exception("Already a subWarehouse of the mainWarehouse you want to add it to.");
            }

            subWarehouse.MainWarehouseId = mainWarehouse.Id;

            subWarehouse.ManagerId = mainWarehouse.ManagerId;

            subWarehouse.Id = Guid.NewGuid();

            _context.Warehouses.Add(subWarehouse);
        }

        public void UpdateWarehouse(Warehouse warehouse)
        {
            //
        }

        public void RemoveWarehouse(Warehouse warehouse)
        {
            if (warehouse == null)
            {
                throw new ArgumentNullException(nameof(warehouse));
            }

            _context.Warehouses.Remove(warehouse);
        }

        public bool WarehouseExists(Guid warehouseId)
        {
            if (warehouseId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(warehouseId));
            }

            return _context.Warehouses.Any(w => w.Id == warehouseId);
        }

        public bool Save()
        {
            return _context.SaveChanges() >= 0;
        }
    }
}