﻿using JWT_NET5.Etities;

namespace JWT_NET5.Srevices.ManagersRepository
{
    public interface IManagerRepository
    {
        Task<IEnumerable<Manager>> GetManagersAsync();
        Task<Manager> GetManagerAsync(Guid managerId);

        // void RemoveManager(Manager manager);
        void UpdateManager(Manager manager);

        Task<bool> ManagerExistsAsync(Guid managerId);
    }
}
