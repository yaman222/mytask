﻿using JWT_NET5.DpContexts;
using JWT_NET5.Etities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace JWT_NET5.Srevices.ManagersRepository
{
    public class ManagerRepository : IManagerRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<Manager> _userManager;
        public ManagerRepository(ApplicationDbContext context, UserManager<Manager> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<Manager> GetManagerAsync(Guid managerId)
        {
            if (managerId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(managerId));
            }

            return await _userManager.FindByIdAsync(managerId.ToString());
        }

        public async Task<IEnumerable<Manager>> GetManagersAsync()
        {
            return await _userManager.Users.ToListAsync();
        }

        public async Task<bool> ManagerExistsAsync(Guid managerId)
        {
            if (managerId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(managerId));
            }

            var user = await _userManager.FindByIdAsync(managerId.ToString());

            if (user == null)
            {
                return false;
            }

            return true;
        }

        //public void RemoveManager(Manager manager)
        //{
        //    if (manager == null)
        //    {
        //        throw new ArgumentNullException(nameof(manager));
        //    }

        //    var user = _userManager.Users.FirstOrDefault(u=>u.Id==manager.Id);


        //    _userManager.remover
        //}

        public void UpdateManager(Manager manager)
        {
            //
        }

        public bool Save()
        {
            return _context.SaveChanges() >= 0;
        }
    }
}
