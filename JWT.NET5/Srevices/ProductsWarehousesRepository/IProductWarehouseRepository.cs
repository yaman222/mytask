﻿using JWT_NET5.Etities;

namespace JWT_NET5.Srevices.ProductsWarehousesRepository
{
    public interface IProductWarehouseRepository
    {
        void AddProductToWarehouse(ProductWarehouse ProductWithWarehouse);
        ProductWarehouse GetProductWithWarehouse(Guid productId, Guid warehouseId);
        IEnumerable<ProductWarehouse> GetProductWithWarehouses();
        void UpdateAmountOfProductInWarehouse(Guid productId, Guid warehouseId, double newAmount);
        bool ProductWithWarehouseExists(Guid productId, Guid warehouseId);
    }
}
