﻿using JWT_NET5.DpContexts;
using JWT_NET5.Etities;

namespace JWT_NET5.Srevices.ProductsWarehousesRepository
{
    public class ProductWarehouseRepository:IProductWarehouseRepository
    {
        private readonly ApplicationDbContext _context;

        public ProductWarehouseRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void AddProductToWarehouse(ProductWarehouse ProductWithWarehouse)
        {
            _context.ProductsWarehouses.Add(ProductWithWarehouse);
        }

        public ProductWarehouse GetProductWithWarehouse(Guid productId, Guid warehouseId)
        {
            return _context.ProductsWarehouses.FirstOrDefault(pw => pw.ProductId == productId && pw.WarehouseId == warehouseId);
        }
        public IEnumerable<ProductWarehouse> GetProductWithWarehouses()
        {
            return _context.ProductsWarehouses.ToList();
        }
        public void UpdateAmountOfProductInWarehouse(Guid productId, Guid warehouseId, double newAmount)
        {
            //
        }
        public bool ProductWithWarehouseExists(Guid productId, Guid warehouseId)
        {
            return _context.ProductsWarehouses.Any(pw => pw.ProductId == productId && pw.WarehouseId == warehouseId);
        }
    }
}
