﻿using JWT_NET5.Etities;

namespace JWT_NET5.Srevices.ProductsRepository
{
    public interface IProductRepository
    {
        IEnumerable<Product> GetProducts();
        Product GetProduct(Guid productId);
        void AddProduct(Product product);
        void updateProduct(Product product);
        void RemoveProduct(Product product);
        bool ProductExists(Guid productId);
    }
}
