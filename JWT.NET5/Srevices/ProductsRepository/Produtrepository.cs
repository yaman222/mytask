﻿using JWT_NET5.DpContexts;
using JWT_NET5.Etities;

namespace JWT_NET5.Srevices.ProductsRepository
{
    public class Produtrepository:IProductRepository
    {
        private readonly ApplicationDbContext _context;


        public Produtrepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Product> GetProducts()
        {
            return _context.Products.ToList();
        }

        public Product GetProduct(Guid productId)
        {
            return _context.Products.FirstOrDefault(p => p.Id == productId);
        }
       
        public bool ProductExists(Guid productId)
        {
            return _context.Products.Any(p => p.Id == productId);
        }

        public void AddProduct(Product product)
        {
            _context.Products.Add(product);
        }

        public void updateProduct(Product product)
        {
            //
        }

        public void RemoveProduct(Product product)
        {
            _context.Products.Remove(product);
        }
    }
}
