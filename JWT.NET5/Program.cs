using JWT_NET5.DpContexts;
using JWT_NET5.Helpers;
using JWT_NET5.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.OpenApi.Models;
using JWT_NET5.Etities;
using JWT_NET5.Srevices.Authentication;
using JWT_NET5.Srevices.WarehousesRepository;
using JWT_NET5.Srevices.ProductsRepository;
using JWT_NET5.Srevices.ProductsWarehousesRepository;
using JWT_NET5.Srevices.Operation;
using JWT_NET5.Srevices;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.


builder.Services.AddControllers();

builder.Services.Configure<JWT_NET5.Helpers.JWT>(builder.Configuration.GetSection("JWT"));

builder.Services.AddDbContext<ApplicationDbContext>(options =>
{
    options.UseNpgsql(@"Host=localhost;Port=5432;Username=postgres;Password=0000;Database=JWTDB;");
});

builder.Services.AddIdentity<Manager, IdentityRole>()
    .AddEntityFrameworkStores<ApplicationDbContext>()
    .AddDefaultTokenProviders();

builder.Services.AddScoped<IWarehouseRepository,WarehouseRepository>();

builder.Services.AddScoped<IProductRepository,Produtrepository>();

builder.Services.AddScoped<IProductWarehouseRepository, ProductWarehouseRepository>();

builder.Services.AddScoped<IOperationRepository, OperationRepository>();

builder.Services.AddTransient<IAuthService, AuthService>();
builder.Services.AddJwtBearerAuthentication(builder.Configuration);
builder.Services.AddScoped<IUserService, UserService>();

builder.Services.AddHttpContextAccessor();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(o =>
{
    var securitySchema = new OpenApiSecurityScheme()
    {
        Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.Http,
        Scheme = "bearer",
        Reference = new OpenApiReference
        {
            Type = ReferenceType.SecurityScheme,
            Id = "Bearer"
        }
    };

    o.AddSecurityDefinition("Bearer", securitySchema);

    o.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        { securitySchema, new[] { "Bearer" } }
    });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();