﻿using JWT_NET5.Etities;
using JWT_NET5.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace JWT_NET5.DpContexts
{
    public class ApplicationDbContext : IdentityDbContext<Manager,IdentityRole<Guid>,Guid>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            :base(options)
        {
 
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }
        public DbSet<ProductWarehouse> ProductsWarehouses { get; set; }
        public DbSet<Operations> Operations { get; set; }
        public DbSet<ProductOperation> ProductsOperations { get; set; }
    }
}
