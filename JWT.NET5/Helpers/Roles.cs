﻿namespace JWT_NET5.Helpers
{
    public static class Roles
    {
        public const string Manager = "Manager";
        public const string User = "User";
    }
}
