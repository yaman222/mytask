﻿namespace JWT_NET5.Dtos
{
    public class ManagerDto:ManagerForManipulationDto
    {
        public string Email { get; set; }
        public Guid Id { get; set; }
    }
}
