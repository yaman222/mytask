﻿using JWT_NET5.Etities;

namespace JWT_NET5.Dtos
{
    public class ImportDto
    {

        public Guid WarehouseId { get; set; }

        public IEnumerable<ProductOperation> ProductAmount { get; set; }
            = new List<ProductOperation>();
    }
}
