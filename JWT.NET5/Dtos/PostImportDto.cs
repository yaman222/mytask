﻿namespace JWT_NET5.Dtos
{
    public class PostImportDto
    {
        public Guid ManagerId { get; set; }

        public ImportDto ImportDto { get; set; }
    }
}
