﻿using JWT_NET5.Etities;
using System.Linq.Expressions;

namespace JWT_NET5.Dtos
{
    public class GetPostWarehouseDto:PostWarehouseDto
    {
        public Guid Id { get; set; }


        public static Expression<Func<Warehouse, GetPostWarehouseDto>> Selector() => w
            => new GetPostWarehouseDto()
            {
                Id = w.Id,
                Name = w.Name,
                Address = w.Address,
                ManagerId = w.ManagerId
            };
    }
}
