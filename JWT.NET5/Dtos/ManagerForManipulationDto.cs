﻿using System.ComponentModel.DataAnnotations;

namespace JWT_NET5.Dtos
{
    public class ManagerForManipulationDto
    {
        public string Name { get; set; }

        [EmailAddress]
        public string Email { get; set; }
    }
}
