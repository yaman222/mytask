﻿namespace JWT_NET5.Dtos;

public class WarehouseDto
{
    public string Name { get; set; }

    public string Address { get; set; }
}