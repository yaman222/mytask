﻿namespace JWT_NET5.Dtos
{
    public class MainWarehouseDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }

        public IEnumerable<GetPostWarehouseDto> SubWarehouses { get; set; }=new List<GetPostWarehouseDto>();
    }
}
