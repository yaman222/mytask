﻿using System.IdentityModel.Tokens.Jwt;

namespace JWT_NET5.Models
{
    public class GetMangerDto
    {
        public Guid Id { get; set; }

        public string Message { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public JwtSecurityToken Token { get; set; }
    }
}
