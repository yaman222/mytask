﻿using System.ComponentModel.DataAnnotations;

namespace JWT_NET5.Models
{
    public class RegisterModel
    {

        [Required, StringLength(50)]
        public string UserName { get; set; }

        [Required, StringLength(128)]
        public string Email { get; set; }

        [Required, StringLength(256)]
        public string Password { get; set; }

    }
}
