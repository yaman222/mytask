﻿using System.ComponentModel.DataAnnotations;

namespace JWT_NET5.Models
{
    public class LoginDto
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
