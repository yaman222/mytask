﻿namespace JWT_NET5.Models
{
    public class AddRoleToUserModel
    {
        public string UserId { get; set; }
        public string RoleName { get; set; }
    }
}
